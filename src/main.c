#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/keysym.h>
#include <X11/Xatom.h>
#include <GL/glcorearb.h>
#include <GL/glx.h>
#include <GL/glxext.h>

#define _XR_COUNTOF(anArray) (sizeof(anArray) / sizeof(anArray[0]))

typedef enum { xrResult_Success, xrResult_Error } xrResult_t;

static void xrDebugPrintf(const char *file, const int32_t line, const char *fmt,
                          ...) {
  fprintf(stderr, "\n[%s : %d]\n", file, line);

  char tmpBuffer[1024];
  va_list argsPtr;
  va_start(argsPtr, fmt);
  vsnprintf(tmpBuffer, _XR_COUNTOF(tmpBuffer), fmt, argsPtr);
  va_end(argsPtr);

  fputs(tmpBuffer, stderr);
  fflush(stderr);
}

#define DEBUG_PRINTF(msg, ...)                                                 \
  do {                                                                         \
    xrDebugPrintf(__FILE__, __LINE__, msg, ##__VA_ARGS__);                     \
  } while (0)

#define XR_SCOPED(destroyFunc) __attribute__((__cleanup__(destroyFunc)))

struct {
#define GL_FNPROTO(FuncType, FuncName) FuncType FuncName;
#include "opengl.prototypes.h"
#undef GL_FNPROTO
} gl;

xrResult_t xrLoadOpenGL() {
#define GL_FNPROTO(FuncType, FuncName)                                         \
  do {                                                                         \
    assert(gl.FuncName == NULL);                                               \
    gl.FuncName =                                                              \
        (FuncType)glXGetProcAddress((const GLubyte *)("gl" #FuncName));        \
    if (!gl.FuncName) {                                                        \
      DEBUG_PRINTF("Failed to load function %s", "gl" #FuncName);              \
      return xrResult_Error;                                                   \
    }                                                                          \
  } while (0);

#include "opengl.prototypes.h"
#undef GL_FNPROTO

  return xrResult_Success;
}

void xrGLDeleter_Buffer(GLuint *pbuff) { gl.DeleteBuffers(1, pbuff); }
void xrGLDeleter_VertexArray(GLuint *vao) { gl.DeleteVertexArrays(1, vao); }
void xrGLDeleter_Sampler(GLuint *samplerId) { gl.DeleteSamplers(1, samplerId); }
void xrGLDeleter_Texture(GLuint *textureId) { gl.DeleteTextures(1, textureId); }
GLuint xrMakeShaderProgram(const GLenum progType, const char **srcStrings,
                           const size_t stringsCount) {}

struct xrUpdateContext_t {
  float ucDeltaTime;
  void *ucUserData;
};

struct xrDrawContext_t {
  int32_t dcSurfaceWidth;
  int32_t dcSurfaceHeight;
  void *dcUserData;
};

struct xrBasicWindow_t {
  struct {
    void *_updateArgs;
    void (*_update)(const struct xrUpdateContext_t *);

    void *_drawArgs;
    void (*_draw)(const struct xrDrawContext_t *);
  } _events;
  Display *_display;
  int32_t _screen;
  Window _window;
  GLXContext _context;
  Atom _atomDelete;
  int32_t _shouldClose;
  GLXContext _glContext;
  int32_t _wndWidth;
  int32_t _wndHeight;
};

static void xrDestroy_GLXFBConfig(GLXFBConfig **ptr) { XFree(*ptr); }
static void xrDestroy_XVisualInfo(XVisualInfo **visual) { XFree(*visual); }
static void xrDestroy_XSizeHints(XSizeHints **hints) { XFree(*hints); }
static void xrDestroy_XWMHints(XWMHints **hints) { XFree(*hints); }

static xrResult_t
xrBasicWindow_InitializeGLContext(struct xrBasicWindow_t *wndPtr,
                                  GLXFBConfig framebufferConfig) {
  PFNGLXCREATECONTEXTATTRIBSARBPROC glXCreateContextAttribsARB =
      (PFNGLXCREATECONTEXTATTRIBSARBPROC)glXGetProcAddress(
          (const GLubyte *)"glXCreateContextAttribsARB");

  if (!glXCreateContextAttribsARB) {
    DEBUG_PRINTF("glXCreateContextAttribsARB extension not present!");
    return xrResult_Error;
  }

  const int32_t contextProperties[] = {
      GLX_CONTEXT_MAJOR_VERSION_ARB,
      4,
      GLX_CONTEXT_MINOR_VERSION_ARB,
      5,
      GLX_CONTEXT_FLAGS_ARB,
      GLX_CONTEXT_DEBUG_BIT_ARB | GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
      GLX_CONTEXT_PROFILE_MASK_ARB,
      GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
      GLX_RENDER_TYPE,
      GLX_RGBA_TYPE,
      None};

  wndPtr->_glContext = glXCreateContextAttribsARB(
      wndPtr->_display, framebufferConfig, NULL, True, contextProperties);

  if (!wndPtr->_glContext) {
    DEBUG_PRINTF("Failed to create OPENGL context!");
    return xrResult_Error;
  }

  glXMakeContextCurrent(wndPtr->_display, wndPtr->_window, wndPtr->_window,
                        wndPtr->_glContext);

  return xrLoadOpenGL();
}

xrResult_t xrBasicWindow_Initialize(struct xrBasicWindow_t *wndPtr) {
  memset(wndPtr, 0, sizeof(*wndPtr));

  wndPtr->_display = XOpenDisplay(NULL);
  if (!wndPtr->_display) {
    DEBUG_PRINTF("\nFailed to open display !");
    return xrResult_Error;
  }

  wndPtr->_screen = DefaultScreen(wndPtr->_display);
  assert(wndPtr->_screen != -1);

  {
    int32_t errorBase = 0;
    int32_t resultBase = 0;
    if (glXQueryExtension(wndPtr->_display, &errorBase, &resultBase) != True) {
      DEBUG_PRINTF("Missing GLX extension !!");
      return xrResult_Error;
    }
  }

  {
    int32_t glxMajor = 0;
    int32_t glxMinor = 0;
    if (!glXQueryVersion(wndPtr->_display, &glxMajor, &glxMinor) ||
        ((glxMajor != 1) && (glxMinor < 4))) {
      DEBUG_PRINTF("Wrong glx version : need [1.4], have [%d.%d]", glxMajor,
                   glxMinor);
      return xrResult_Error;
    }

    DEBUG_PRINTF("GLX library vendor : %s",
                 glXGetClientString(wndPtr->_display, GLX_VENDOR));
    DEBUG_PRINTF("GLX version %s",
                 glXGetClientString(wndPtr->_display, GLX_VERSION));
    DEBUG_PRINTF("Extensions list %s",
                 glXQueryExtensionsString(wndPtr->_display, wndPtr->_screen));
  }

  const int32_t kFBRequiredProps[] = {GLX_DOUBLEBUFFER,
                                      True,
                                      GLX_RED_SIZE,
                                      8,
                                      GLX_GREEN_SIZE,
                                      8,
                                      GLX_BLUE_SIZE,
                                      8,
                                      GLX_ALPHA_SIZE,
                                      8,
                                      GLX_DEPTH_SIZE,
                                      24,
                                      GLX_STENCIL_SIZE,
                                      8,
                                      GLX_RENDER_TYPE,
                                      GLX_RGBA_BIT,
                                      GLX_DRAWABLE_TYPE,
                                      GLX_WINDOW_BIT,
                                      GLX_CONFIG_CAVEAT,
                                      GLX_NONE,
                                      None};

  int32_t supportedConfigsCount = 0;
  GLXFBConfig *supportedConfigs XR_SCOPED(xrDestroy_GLXFBConfig) =
      glXChooseFBConfig(wndPtr->_display, wndPtr->_screen, kFBRequiredProps,
                        &supportedConfigsCount);

  if (supportedConfigsCount == 0) {
    DEBUG_PRINTF("No framebuffers that support the required attributes!");
    return xrResult_Error;
  }

  GLXFBConfig chosenFBConfig = supportedConfigs[0];
  XVisualInfo *chosenVisual XR_SCOPED(xrDestroy_XVisualInfo) =
      glXGetVisualFromFBConfig(wndPtr->_display, chosenFBConfig);

  XSetWindowAttributes windowAttrs = {0};
  windowAttrs.background_pixel = WhitePixel(wndPtr->_display, wndPtr->_screen);
  windowAttrs.event_mask = KeyPressMask | ButtonPressMask | StructureNotifyMask;
  windowAttrs.colormap = XCreateColormap(
      wndPtr->_display, RootWindow(wndPtr->_display, wndPtr->_screen),
      chosenVisual->visual, AllocNone);

  const unsigned long kValueMask = CWBackPixel | CWEventMask | CWColormap;

  wndPtr->_window = XCreateWindow(
      wndPtr->_display, RootWindow(wndPtr->_display, wndPtr->_screen), 0, 0,
      1024, 1024, 0, chosenVisual->depth, InputOutput, chosenVisual->visual,
      kValueMask, &windowAttrs);

  if (!wndPtr->_window) {
    DEBUG_PRINTF("Failed to create window!");
    return xrResult_Error;
  }

  XSizeHints *wndSizeHints XR_SCOPED(xrDestroy_XSizeHints) = XAllocSizeHints();
  assert(wndSizeHints != NULL);
  wndSizeHints->flags = PPosition | PSize | PMinSize;
  wndSizeHints->min_width = 1024;
  wndSizeHints->max_width = 1024;
  wndSizeHints->base_width = 1024;
  wndSizeHints->base_height = 1024;

  XWMHints *wndWMHints XR_SCOPED(xrDestroy_XWMHints) = XAllocWMHints();
  assert(wndWMHints != NULL);
  wndWMHints->flags = StateHint | InputHint;
  wndWMHints->initial_state = NormalState;
  wndWMHints->input = True;

  const char *kWindowName = "OpenGL basic window!";
  XTextProperty windowName;
  if (!XStringListToTextProperty((char **)&kWindowName, 1, &windowName)) {
    DEBUG_PRINTF("Failed to set name property!");
    return xrResult_Error;
  }

  XTextProperty iconicName;
  if (!XStringListToTextProperty((char **)&kWindowName, 1, &iconicName)) {
    DEBUG_PRINTF("Failed to set name property!");
    return xrResult_Error;
  }

  XSetWMProperties(wndPtr->_display, wndPtr->_window, &windowName, &iconicName,
                   NULL, 0, wndSizeHints, wndWMHints, NULL);

  wndPtr->_atomDelete =
      XInternAtom(wndPtr->_display, "WM_DELETE_WINDOW", False);
  XSetWMProtocols(wndPtr->_display, wndPtr->_window, &wndPtr->_atomDelete, 1);

  //
  // set full screen

  if (xrBasicWindow_InitializeGLContext(wndPtr, chosenFBConfig) !=
      xrResult_Success) {
    return xrResult_Error;
  }

  XMapRaised(wndPtr->_display, wndPtr->_window);

  return xrResult_Success;
}

void xrBasicWindow_Destroy(struct xrBasicWindow_t *wndPtr) {
  DEBUG_PRINTF("%s", __PRETTY_FUNCTION__);
  if (!wndPtr)
    return;

  if (wndPtr->_context) {
    glXDestroyContext(wndPtr->_display, wndPtr->_context);
  }

  if (wndPtr->_window) {
    XDestroyWindow(wndPtr->_display, wndPtr->_window);
  }

  if (wndPtr->_display) {
    XCloseDisplay(wndPtr->_display);
  }
}

void xrBasicWindow_ClientMessageEvent(struct xrBasicWindow_t *wndPtr,
                                      const XClientMessageEvent *clientMsg) {
  if (clientMsg->data.l[0] == wndPtr->_atomDelete) {
    wndPtr->_shouldClose = 1;
  }
}

void xrBasicWindow_ConfigureEvent(struct xrBasicWindow_t *wndPtr,
                                  const XConfigureEvent *cfgEvent) {
  wndPtr->_wndWidth = cfgEvent->width;
  wndPtr->_wndHeight = cfgEvent->height;
}

void xrBasicWindow_MessageLoop(struct xrBasicWindow_t *wndPtr) {
  while (!wndPtr->_shouldClose) {
    while (!wndPtr->_shouldClose &&
           XEventsQueued(wndPtr->_display, QueuedAfterFlush)) {
      XEvent wndEvent;
      XNextEvent(wndPtr->_display, &wndEvent);

      switch (wndEvent.type) {
      case ClientMessage:
        xrBasicWindow_ClientMessageEvent(wndPtr, &wndEvent.xclient);
        break;

      case ConfigureNotify:
        xrBasicWindow_ConfigureEvent(wndPtr, &wndEvent.xconfigure);
        break;

      default:
        break;
      }
    }

    if (wndPtr->_events._update) {
      struct xrUpdateContext_t updateContext = {
          .ucDeltaTime = 0.0f, .ucUserData = wndPtr->_events._updateArgs};

      wndPtr->_events._update(&updateContext);
    }

    if (wndPtr->_events._draw) {
      struct xrDrawContext_t drawContext = {
          .dcSurfaceWidth = wndPtr->_wndWidth,
          .dcSurfaceHeight = wndPtr->_wndHeight,
          .dcUserData = wndPtr->_events._updateArgs};

      wndPtr->_events._draw(&drawContext);
    }

    glXSwapBuffers(wndPtr->_display, wndPtr->_window);
  }
}

void xrBasicWindow_SetUpdateCallback(
    struct xrBasicWindow_t *wndPtr, void *callbackContext,
    void (*fnUpdatePtr)(const struct xrUpdateContext_t *)) {

  wndPtr->_events._updateArgs = callbackContext;
  wndPtr->_events._update = fnUpdatePtr;
}

void xrBasicWindow_SetDrawCallback(
    struct xrBasicWindow_t *wndPtr, void *callbackContext,
    void (*funDraw)(const struct xrDrawContext_t *)) {

  wndPtr->_events._drawArgs = callbackContext;
  wndPtr->_events._draw = funDraw;
}

struct xrGLDemo {};

void xrGLDemo_Draw(const struct xrDrawContext_t *ctx) {
  struct xrGLDemo *obj = (struct xrGLDemo *)ctx->dcUserData;
  const float surfaceWidth = (float)ctx->dcSurfaceWidth;
  const float surfaceHeight = (float)ctx->dcSurfaceHeight;

  gl.ViewportIndexedf(0, 0.0f, 0.0f, surfaceWidth, surfaceHeight);
  const float kClearColor[] = {0.5f, 1.0f, 0.5f, 1.0f};
  gl.ClearNamedFramebufferfv(0, GL_COLOR, 0, kClearColor);
  const float kDepthClearVal = 1.0f;
  gl.ClearNamedFramebufferfv(0, GL_DEPTH, 0, &kDepthClearVal);
}

int main(int argc, char **argv) {

  struct xrBasicWindow_t mainWindow XR_SCOPED(xrBasicWindow_Destroy);
  if (xrBasicWindow_Initialize(&mainWindow) != xrResult_Success) {
    DEBUG_PRINTF("Failed to initialize window!");
    return EXIT_FAILURE;
  }

  struct xrGLDemo simpleDemo;
  xrBasicWindow_SetDrawCallback(&mainWindow, &simpleDemo, &xrGLDemo_Draw);

  xrBasicWindow_MessageLoop(&mainWindow);

  return 0;
}
